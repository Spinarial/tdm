import json
import time
import os
import re

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

import PIL
from PIL import Image
from PIL.ExifTags import TAGS
import json
from PIL import Image
from matplotlib import pyplot as plot
import os.path
import sklearn
import pandas as pd
import glob

METADATA = "../images_metadata"
IMAGES = '../images'
PROGRESS = '../progress'

all_files = glob.glob(METADATA + "/*.csv")

li = []

# TODO ------------- We stopped here --------------------
for filename in all_files:
    print("Doing " + filename)
    df = pd.read_csv(filename, index_col=None, header=None, sep=",")
    li.append(df)

df = pd.concat(li, axis=0, ignore_index=True)