import copy
import os

from sklearn.feature_extraction import DictVectorizer

from sklearn import svm

from download.fileManager import FileManager
import json

from sklearn import tree
import graphviz
import pydotplus
from IPython.display import Image, display


class User:
    """
    Define the data and load it from file if it exist
    """

    def __init__(self):
        self.liked = []
        self.disliked = []

        if os.path.isfile(FileManager.USER_PROFILES + '/user.json'):
            userFile = open(FileManager.USER_PROFILES + '/user.json', 'r')

            try:
                data = json.loads(userFile.read())
                self.liked = data['liked']
                self.disliked = data['disliked']
            except:
                return


    """
    Add a liked image
    """
    def likeImage(self, image):
        self.liked.append(image.__dict__)

    """
    Add a disliked image
    """

    def dislikeImage(self, image):
        self.disliked.append(image.__dict__)

    """
    Save the user profil into the user.json file
    """

    def saveUserProfile(self):
        self.likedData = open(FileManager.USER_PROFILES + '/user.json', "w+")
        self.likedData.write(json.dumps({'liked': self.liked, 'disliked': self.disliked}))
        self.likedData.close()

    """
    WIP
    Predict the affinity of the user for an image
    """

    def predictUserLiking(self, image):

        likedMetadata = []

        data = []
        result = []

        vec = DictVectorizer(sparse=False)

        for image in self.liked:
            metadataFile = open(image['metadataPath'])

            metadata = metadataFile.read()

            if not metadata or metadata == 'None':
                continue

            metadata = json.loads(metadata)

            # Add the data after the vector of the dict has been found
            data.append(vec.fit_transform([metadata]))
            result.append('liked')

        for image in self.disliked:
            metadataFile = open(image['metadataPath'])

            metadata = metadataFile.read()

            if not metadata or metadata == 'None':
                continue

            metadata = json.loads(metadata)

            # toScan = {}
            # for (targetindex, target) in metadata.items():
            #     if isinstance(target, str):
            #         toScan[targetindex] = vec.fit_transform({(targetindex): target})
            #     else:
            #         toScan[targetindex] = target

            data.append(vec.fit_transform([metadata]))
            result.append('disliked')

        # We need to delete vectors that aren't the same size as the other (for some reasons ?)
        maxSize = 0
        for i in range(len(data)):
            dataToMeasure = data[i]
            if maxSize < dataToMeasure.size:
                maxSize = dataToMeasure.size

        tempData = copy.copy(data)
        for i in range(len(tempData)):
            dataToMeasure = tempData[i]

            if not maxSize == dataToMeasure.size:
                del data[data.index(dataToMeasure)]

        clf = svm.SVC()
        # TODO WIP this is where we stopped developement, we couldn't manage to fit the data into a SVC.

        dtc = tree.DecisionTreeClassifier()
        result = dtc.fit(data, result)

        dot_data = tree.export_graphviz(dtc, out_file=None,
                                        feature_names=['column1', 'column2'],
                                        filled=True, rounded=True,
                                        class_names=['class1', 'class2']
                                        )
        graph = graphviz.Source(dot_data)
        pydot_graph = pydotplus.graph_from_dot_data(dot_data)
        img = Image(pydot_graph.create_png())
        display(img)
