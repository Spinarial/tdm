
# Traitement de données massives
## Projet de fin de module

#### Etudiants:
- Robin Chamouret
- Rémi Quemin
- Eloi Desbrosses

### Cahier des charges

* [ ] Système de recommandation d'image utilisant une méthodologie de sélection vue en cours.
* [X] Utilisation de multiples instances via un réseau docker pour répartir la charge
* [X] Utilisation de `map()` ou `filter()`
