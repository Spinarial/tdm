#!/bin/bash

pip install --no-cache-dir -r /app/metadata/requirements.txt
python3 /app/metadata/metadata.py