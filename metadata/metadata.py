import csv
import json
import shutil
import time
import os
import topColors

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

import PIL
from PIL import Image
from PIL.ExifTags import TAGS
import json
from PIL import Image
from matplotlib import pyplot as plot
from decouple import config

import os.path

COLORS = "colors"
METADATA = "images_metadata"
IMAGES = 'images'
PROGRESS = 'progress'

# Variable de loop global
LOOP = True
FORCE_METADATA_GENERATOR = bool(int(config('FORCE_METADATA_GENERATOR')))

"""
ListenHandler for the file watch hook. Used to trigger a function when a progress file is created
"""
class ListenerHandler(FileSystemEventHandler):
    def on_created(self, event):
        partitioned_string = event.src_path.rpartition('.done')
        next_partitioned_string = partitioned_string[0].rpartition('/')
        filename = next_partitioned_string[len(next_partitioned_string) - 1]
        if filename == "download":
            print("Done !")
            global LOOP
            LOOP = False
            return
        getMetadata(str(IMAGES) + "/" + str(filename) + ".jpg")


"""
Read an image and extract it's metadata in a csv format. Used to also get the top colors of the image
"""
def getMetadata(path):
    print('Found new picture at ' + path)
    print("Analysing best colors...")

    path = path.replace('\\', '/')

    fileName = path.split('/')
    fileName = fileName[len(fileName) - 1]

    if os.path.isfile(METADATA + "/" + fileName + ".csv") and (not FORCE_METADATA_GENERATOR):
        print("File " + str(fileName) + ".csv is  already generated. Skipping.")
        return

    # We open up the image
    img = PIL.Image.open(path)
    exif_data = img._getexif()

    if not exif_data or exif_data is None:
        exif_data = {}

    metadateResult = {}

    for tag_id in exif_data:
        # get the tag name, instead of human unreadable tag id
        tag = TAGS.get(tag_id, tag_id)
        data = exif_data.get(tag_id)

        try:
            # To be sure it can be used by json we try first if json can load the data
            # If it cannot, it cannot be used as a feature
            json.dumps(data)
            metadateResult[tag] = data
        except:
            continue

    # TODO incorporate color values in parsable format
    # metadateResult['colors_data'] = self.getColorsData()

    if len(metadateResult) > 0:
        metadataFile = open(METADATA + "/" + fileName + ".csv", "w+")

        result = {}

        if metadateResult.get('Make'):
            result['Make'] = metadateResult.get('Make')

        if metadateResult.get('Software'):
            result['Software'] = metadateResult.get('Software')

        if metadateResult.get('Orientation'):
            result['Orientation'] = str(metadateResult.get('Orientation'))

        if metadateResult.get('ExifImageWidth'):
            result['ExifImageWidth'] = metadateResult.get('ExifImageWidth')

        if metadateResult.get('ExifImageHeight'):
            result['ExifImageHeight'] = metadateResult.get('ExifImageHeight')

        result['BestColors'] = topColors.topImageColors(img, COLORS + "/" + fileName + ".png")[:5]

        writter = csv.writer(metadataFile, delimiter=',')

        # We only write a few properties
        for property in result:
            writter.writerow((property, result[property]))

        print("File generated at " + COLORS + "/" + fileName + ".png")
        metadataFile.close()


if __name__ == '__main__':

    """
    Generating the folders we're gonna use during the app life
    """
    if not os.path.isdir(METADATA):
        os.mkdir(METADATA)

    max = 1
    while ((not os.path.isdir(PROGRESS))) and max <= 100:
        print("Folder " + PROGRESS + " does not exist. Waiting. Try: " + str(max))
        time.sleep(1)
        max += 1

    if max > 100:
        print("Folder " + PROGRESS + " never showed up. Exiting.")
        exit(-1)

    if os.path.isdir(COLORS):
        shutil.rmtree(COLORS)

    os.makedirs(COLORS)

    observer = Observer()
    observer.schedule(ListenerHandler(), PROGRESS + "/")
    observer.start()

    print("Listening for new images..")
    try:
        while LOOP:
            time.sleep(1)
    finally:
        observer.stop()
        observer.join()
