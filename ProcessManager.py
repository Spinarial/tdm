import time

import psutil


class ProcessManager:

    def killImageProcess(self):
        # hide image
        for proc in psutil.process_iter():
            if proc.name() == "display":
                proc.kill()
