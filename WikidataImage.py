import urllib.request
import PIL
from PIL import Image
from PIL.ExifTags import TAGS
import json
from PIL import Image
from matplotlib import pyplot as plot
import os.path


class WikidataImage:
    """
    Download the image if it's not there alrady.
    And scan the file for some metadata
    """

    def __init__(self, url, name):
        self.url = url
        self.name = name + ".jpg"  # It's not really clean to pass every picture to .jpg, but it'll do for now
        self.fullPath = fileManager.FileManager.IMAGES + "/" + self.name

        self.metadataPath = fileManager.FileManager.IMAGES_METADATA + "/" + self.name + '.json'

        if os.path.isfile(self.fullPath):
            print("Image " + self.name + " already exist. Skipping.")
            self.metadata = self.getMetadata()
            return
        else:
            print("New image " + self.name + ". Downloading.")

        self.file = open(self.fullPath, 'a+')

        # prevent wikidata from blocking our bot
        opener = urllib.request.build_opener()
        opener.addheaders = [('User-agent',
                              'BotMoto (rabinremo@gmail.com) generic-library/0.0')]
        urllib.request.install_opener(opener)
        urllib.request.urlretrieve(url, self.fullPath)

        self.file.close()

        self.metadata = self.getMetadata()

    """
    Scan the file for some EXIF metadata, return a comprehensible list of metadata in key->value form
    """

    def getMetadata(self):
        img = PIL.Image.open(self.fullPath)
        exif_data = img._getexif()

        if not exif_data or exif_data is None:
            exif_data = {}

        metadateResult = {}

        for tag_id in exif_data:
            # get the tag name, instead of human unreadable tag id
            tag = TAGS.get(tag_id, tag_id)
            data = exif_data.get(tag_id)
            ## TODO find a way to keep the bytes somehow
            # decode bytes
            # if isinstance(data, bytes):
            # encoded = base64.b64encode(data)
            # data = encoded.decode('ascii')
            # continue

            try:
                # To be sure it can be used by json we try first if json can load the data
                # If it cannot, it cannot be used as a feature
                json.dumps(data)
                metadateResult[tag] = data
            except:
                continue

        # TODO incorporate color values in parsable format
        # metadateResult['colors_data'] = self.getColorsData()

        metadataFile = open(self.metadataPath, "w+")

        # We test if the results are greater than 2 because ultimately, the color_data will be there
        if len(metadateResult) < 2:
            metadataFile.write('None')
            metadataFile.close()
        else:
            result = {}

            if metadateResult.get('Make'):
                result['Make'] = metadateResult.get('Make')

            if metadateResult.get('Software'):
                result['Software'] = metadateResult.get('Software')

            if metadateResult.get('Orientation'):
                result['Orientation'] = str(metadateResult.get('Orientation'))

            if metadateResult.get('ExifImageWidth'):
                result['ExifImageWidth'] = metadateResult.get('ExifImageWidth')

            if metadateResult.get('ExifImageHeight'):
                result['ExifImageHeight'] = metadateResult.get('ExifImageHeight')

            metadataFile.write(json.dumps(result))
            metadataFile.close()

        return metadateResult

    """
    Analyse a file and extract the rgb values of each pixels
    """

    def getColorsData(self):

        imgfile = Image.open(self.fullPath)
        histogram = imgfile.histogram()
        red = histogram[0:255]
        green = histogram[256:511]
        blue = histogram[512:767]
        x = range(255)
        y = []

        for i in x:
            y.append((red[i], green[i], blue[i]))
        figure, axes = plot.subplots()

        axes.set_prop_cycle('color', ['red', 'green', 'blue'])
        plot.plot(x, y)
        plot.savefig(fileManager.FileManager.IMAGES_COLORS + '/' + self.name)
        plot.close()
        return (red, green, blue)
