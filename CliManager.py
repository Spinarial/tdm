from PIL import Image

from numpy import random
from ProcessManager import ProcessManager
from User import User


class CliManager:

    """
    Asks the user to tell them if they like 10 pictures or not
    """
    def buildUserPreferences(self, images):
        print("We are gonna show you 10 pictures. Please follow the instructions.")
        user = User()

        processManager = ProcessManager()

        for i in range(5):
            picture = images[random.randint(0, len(images))]

            img = Image.open(picture.fullPath)
            img.show(picture.fullPath)

            pictureLiked = False
            while not pictureLiked:
                pictureLiked = input("Do you like this picture ? (y/n)")

            if pictureLiked == "y":
                user.likeImage(picture)

            if pictureLiked == "n":
                user.dislikeImage(picture)

            processManager.killImageProcess()

        user.saveUserProfile()
