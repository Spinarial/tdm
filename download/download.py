import json
import os
import shutil
import time
import urllib.request
from decouple import config
import multiprocessing as mp

from slugify import slugify

# Constants and configuration variables
IMAGES = 'images'
PROGRESS = 'progress'

FORCE_DOWNLOAD = bool(int(config('FORCE_DOWNLOAD')))
FORCE_METADATA_GENERATOR = bool(int(config('FORCE_METADATA_GENERATOR')))

"""
Download the image based on a (file_url, name) input
"""
def download_image(tuple):
    file_url, name = tuple

    file_path = IMAGES + "/" + name + ".jpg"

    # If the FORCE_DOWNLOAD option isn't true and the file exist we can skip it
    if not FORCE_DOWNLOAD and os.path.isfile(file_path):
        print("File " + name + ".png already exist. Skipping.")

        # If FORCE_METADATA_GENERATOR is set to True we mimick the creation of a progress file for the event hook of the metadata service
        if FORCE_METADATA_GENERATOR:
            print("Forcing metadata generation process")
            time.sleep(1)
            progressFile = open(PROGRESS + "/" + name + ".done", 'w+')
            progressFile.write(name + ".jpg")
            progressFile.close()

        return

    # prevent wikidata from blocking our bot
    opener = urllib.request.build_opener()
    opener.addheaders = [('User-agent', 'BotMoto (rabinremo@gmail.com) generic-library/0.0')]
    urllib.request.install_opener(opener)
    print("Downloading " + name + "...")

    # Open and close file for event trigger through
    file = open(file_path, 'a+')
    urllib.request.urlretrieve(file_url, file_path)
    file.close()

    progressFile = open(PROGRESS + "/" + name + ".done", 'w+')
    progressFile.close()

    print("Downloaded at " + file_path)
    return True

"""
Start multithreading downloading process based on options
"""
def start_download_processes(result):
    """
    Get number of available CPU
    """
    cpu_count = 0
    try:
        cpu_count = int(config('WORKING_CPU'))
    except:
        print('WORKING_CPU is not defined in the .env file. You must set a number of WORKING_CPU.')
        exit(-1)

    if cpu_count < 1:
        print("Cannot download pictures with 0 CPU allocated")
        exit(-1)

    with mp.Pool(processes=cpu_count) as pool:
        status = pool.map(download_image, result)


if __name__ == '__main__':

    """
    Generating the folders we're gonna use during the app life
    """
    if not os.path.isdir(IMAGES):
        os.mkdir(IMAGES)

    if os.path.isdir(PROGRESS):
        shutil.rmtree(PROGRESS)

    os.makedirs(PROGRESS)

    """
    Get wikidata url to query on
    """
    try:
        WIKIDATA_URL = config('WIKIDATA_URL')
    except:
        print('WIKIDATA_URL is not defined in the .env file. You must set a WIKIDATA_URL.')
        exit(-1)

    if FORCE_DOWNLOAD:
        print("FORCE_DOWNLOAD activated.")

    if FORCE_METADATA_GENERATOR:
        print("FORCE_METADATA_GENERATOR activated.")

    response = urllib.request.urlopen(WIKIDATA_URL)
    responsedata = json.loads(response.read().decode('utf-8'))
    responsedata = responsedata['results']['bindings']

    result = list(
        map(
            lambda data: (data['image']['value'], slugify(data['itemLabel']['value'])),
            responsedata
        )
    )

    start_download_processes(result)

    # Open and close file for event trigger through
    file = open(PROGRESS + "/download.done", 'w+')
    file.close()
    print("Done !")

    # while True:
    #     print(os.listdir(PROGRESS))